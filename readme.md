# Debug Bar Keyboard Shortcut #

Nothing fancy at this stage, just a simple script that adds the **Control + Shift + D** keyboard
shortcut for toggling the debug bar panel.