<?php
/*
Plugin Name: Debug Bar Keyboard Shortcut
Plugin URI: http://philsmart.me
Description: Adds the keyboard shortcut <strong>Ctrl + Shift + D</strong> to activate the debug bar panel.
Description:
Version: 0.1.1
Author: Phil Smart
Author URI: http://philsmart.me
License:
*/

add_action('admin_footer', 'debugBarKeyboardShortcut');
add_action('wp_footer', 'debugBarKeyboardShortcut');
/**
 * Adds debug bar keyboard shortcut – shift + control + d
 */
function debugBarKeyboardShortcut()
{
    ?>
    <script>
        document.onkeydown = function (e)
        {
            if(wpDebugBar && e.ctrlKey && e.shiftKey && e.keyCode == 68)
                wpDebugBar.toggle.visibility();
        }
    </script>
    <?php
}